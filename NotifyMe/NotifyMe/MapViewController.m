//
//  MapViewController.m
//  NotifyMe
//
//  Created by Lee on 1/11/14.
//  Copyright (c) 2013 Scarib Tech. All rights reserved.
//

#import "MapViewController.h"
#import "SWRevealViewController.h"
#import "Annotation.h"
#import "AFNetworking.h"
#import "LocationController.h"
#import <MapKit/MapKit.h>
#import "Building.h"

@interface MapViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end


#define REV_LATITUDE 43.650709;
#define REV_LONGITUDE -116.281167;
#define THE_SPAN 0.01f;

@implementation MapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    MKCoordinateRegion myregion;
    CLLocationCoordinate2D center;
    center.latitude = REV_LATITUDE;
    center.longitude = REV_LONGITUDE;
    
    MKCoordinateSpan span;
    span.latitudeDelta = THE_SPAN;
    span.longitudeDelta = THE_SPAN;
    
    myregion.center = center;
    myregion.span = span;
    
    //The code below handles the setting of information to my pin
    //As well as placing the pin on the map
    
    Building *revcoords = [[Building alloc] init];
    revcoords.name = @"Revolution Concert House and Event Center";
    revcoords.descripshun = @"4983 Glenwood St. Unit 4, Garden City, ID 83714";
    revcoords.longitude = REV_LONGITUDE;
    revcoords.latitude = REV_LATITUDE;
    
    
    CLLocationCoordinate2D revlocation;
    revlocation.longitude = revcoords.longitude;
    revlocation.latitude = revcoords.latitude;
    
    Annotation * myAnnotation = [Annotation alloc];
    myAnnotation.coordinate = revlocation;
    myAnnotation.title = revcoords.name;
    myAnnotation.subtitle = revcoords.descripshun;
    [self.mapView addAnnotation:myAnnotation];


    
    // Change button color
    _sidebarButton.tintColor = [UIColor colorWithWhite:0.1f alpha:0.9f];
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];

}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  PhotoViewController.h
//  NotifyMe
//
//  Created by Lee on 1/11/14.
//  Copyright (c) 2013 Scarib Tech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (strong, nonatomic) NSString *photoFilename;
@end

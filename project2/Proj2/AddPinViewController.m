//
//  AddPinViewController.m
//  Proj2
//
//  Created by Lee on 10/25/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//

#import "AddPinViewController.h"

@interface AddPinViewController ()

@end

@implementation AddPinViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.userLocation = [LocationController currentLocation];
    
    NSString *filePath = [self documentsPathForFileName: @"image.png"];
    NSData *pngData = [NSData dataWithContentsOfFile:filePath];
    _image = [UIImage imageWithData:pngData];
    [self.imageView setImage:_image];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveButtonPressed:(id)sender
{
    CLLocationCoordinate2D userCoordinate = self.userLocation.coordinate;
    
    Pin *newPin = [NSEntityDescription insertNewObjectForEntityForName:@"Pin" inManagedObjectContext:[DatabaseController managedObjectContext]];
    newPin.pinName = self.pinTitle.text;
    newPin.pinDescription = self.pinDescription.text;
    
    newPin.latitude = [NSNumber numberWithDouble:userCoordinate.latitude];
    newPin.longitude = [NSNumber numberWithDouble:userCoordinate.longitude];
    [DatabaseController save];
    
    
    NSData *pngData = UIImagePNGRepresentation(_image);
    NSString *filePath = [self documentsPathForFileName: @"image.png"];
    [pngData writeToFile:filePath atomically:YES];
}

- (NSString *)documentsPathForFileName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}


- (IBAction)takePhotoButtonPressed:(id)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    _image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self.imageView setImage:_image];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  newTableViewController.m
//  Proj2
//
//  Created by Lee on 10/24/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//

#import "TableViewController.h"

@interface TableViewController ()

@end

@implementation TableViewController

- (void)viewDidLoad {
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(updatePins) name: kAFN object: nil];
    
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    [self updatePins];
}

-(void)updatePins
{
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [PinController getPinCount];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"Cell" forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: NSStringFromClass ([Pin class])];
    
    NSArray *results = [[DatabaseController managedObjectContext] executeFetchRequest: request error: nil];
    
    Pin *currentPin = results[indexPath.row];
    cell.textLabel.text = currentPin.pinName;
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%@", indexPath);
    
    [tableView deselectRowAtIndexPath: indexPath animated: NO];
    return;
}

- (IBAction)refreshButtonPushed:(id)sender
{
    [PinController clearPinsFromDataBase];
    [PinController parseJSONForPins];
    [self updatePins];
    [[NSNotificationCenter defaultCenter] postNotificationName:kRefresh object:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  Pin.m
//  Proj2
//
//  Created by Lee on 10/29/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//

#import "Pin.h"


@implementation Pin

@dynamic latitude;
@dynamic longitude;
@dynamic pinDescription;
@dynamic pinName;

@end

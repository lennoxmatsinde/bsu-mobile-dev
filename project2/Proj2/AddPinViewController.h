//
//  AddPinViewController.h
//  Proj2
//
//  Created by Lee on 10/25/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationController.h"
#import "Pin.h"
#import "DatabaseController.h"
#define kPinSaved @"kPinSaved"

@interface AddPinViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *pinTitle;
@property (weak, nonatomic) IBOutlet UITextField *pinDescription;
@property (weak, nonatomic) IBOutlet UIButton *takePhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (retain, nonatomic) IBOutlet UIImageView *imageView;
@property (assign, nonatomic) CLLocation *userLocation;
@property (retain, nonatomic) UIImage *image;

- (IBAction)saveButtonPressed:(id)sender;
- (IBAction)takePhotoButtonPressed:(id)sender;

@end

//
//  mapViewController.h
//  Proj2
//
//  Created by Lee on 10/24/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "AFNetworking.h"
#import "LocationController.h"
#import "Pin.h"
#import "DatabaseController.h"
#import "PinController.h"
#define kAFN @"kAFN"


@interface mapViewController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *dropPinButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *refreshButton;


@end

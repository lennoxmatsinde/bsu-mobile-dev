//
//  LocationController.m
//
//  Created by Michael Ziray on 2/6/12.
//

#import "LocationController.h"


static LocationController *sharedLocationController = nil;



@interface LocationController()

@property (nonatomic, retain) CLLocationManager *currentLocationManager;

@end



@implementation LocationController


static CLLocation *currentLocation;


+(CLLocation *)currentLocation
{
	return currentLocation;
}

+(LocationController *)sharedLocationController
{
    if( sharedLocationController == nil )
    {
        sharedLocationController = [[LocationController alloc] init];
        sharedLocationController.isReporting = NO;
    }
    
    return sharedLocationController;
}



+(void)startLocationReporting
{
	[[LocationController sharedLocationController] setCurrentLocationManager: [[CLLocationManager alloc] init]];
    
    // Accuracy
	[sharedLocationController.currentLocationManager setDesiredAccuracy: kCLLocationAccuracyKilometer];
    
    // Don't update until device has moved at least 10 meters -- saves battery
	[sharedLocationController.currentLocationManager setDistanceFilter: 10];
    
    
	[sharedLocationController.currentLocationManager setDelegate: sharedLocationController];
	[sharedLocationController.currentLocationManager startUpdatingLocation];
}


+(void)stopLocationReporting
{
    [[LocationController sharedLocationController] setIsReporting: NO];
    [sharedLocationController.currentLocationManager stopUpdatingLocation];
    sharedLocationController.currentLocationManager = nil;
}



- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
		   fromLocation:(CLLocation *)oldLocation
{
	if( currentLocation == nil )
	{
		currentLocation = newLocation;
	}
    
    _isReporting = YES;
	
	// If it's a relatively recent event, turn off updates to save power
    //NSDate* eventDate = newLocation.timestamp;
    //NSTimeInterval lastUpdateInterval = [eventDate timeIntervalSinceNow];
    //NSLog(@"%+.1f", currentLocation.horizontalAccuracy);
    
    
    
    BOOL isInBackground = NO;
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
    {
        isInBackground = YES;
    }
    
    // Handle location updates as normal, code omitted for brevity.
    // The omitted code should determine whether to reject the location update for being too
    // old, too close to the previous one, too inaccurate and so forth according to your own
    // application design.
    
    if (isInBackground)
    {
        [self sendBackgroundLocationToServer:newLocation];
    }
    else
    {
		if( currentLocation != nil )
		{
			currentLocation = nil;
		}
		currentLocation = newLocation;
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName: kLocationDidChange object: currentLocation];
    }
	
}

-(void) sendBackgroundLocationToServer:(CLLocation *)location
{
    UIBackgroundTaskIdentifier backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:
                                                 ^{
                                                     [[UIApplication sharedApplication] endBackgroundTask: backgroundTask];
                                                 }];
    
    
    // AFTER ALL THE UPDATES, close the task
    if (backgroundTask != UIBackgroundTaskInvalid)
    {
        [[UIApplication sharedApplication] endBackgroundTask: backgroundTask];
        backgroundTask = UIBackgroundTaskInvalid;
        NSLog(@"time remaining: %f", [UIApplication sharedApplication].backgroundTimeRemaining);
    }
}






@end

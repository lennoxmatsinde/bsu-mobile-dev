//
//  Pin.h
//  Proj2
//
//  Created by Lee on 10/29/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Pin : NSManagedObject

@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * pinDescription;
@property (nonatomic, retain) NSString * pinName;

@end

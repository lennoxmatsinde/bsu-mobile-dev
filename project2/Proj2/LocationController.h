//
//  LocationController.h
//
//  Created by Michael Ziray on 2/6/12.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>



#define kLocationDidChange @"kLocationDidChange"



@interface LocationController : NSObject <CLLocationManagerDelegate>

@property (nonatomic, assign) BOOL isReporting;

+(LocationController *)sharedLocationController;
+(CLLocation *)currentLocation;

+(void)startLocationReporting;
+(void)stopLocationReporting;

@end

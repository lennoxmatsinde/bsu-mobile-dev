//
//  PinController.m
//  Proj2
//
//  Created by Lee on 10/25/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//

#import "PinController.h"

@implementation PinController

+ (void) parseJSONForPins
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://zstudiolabs.com/labs/compsci402/buildings.json" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        for (NSDictionary *building in responseObject){
            
            NSDictionary *buidlingLocation = building[@"location"];
            NSLog(@"location %@", buidlingLocation);
            
            Pin *newPin = [NSEntityDescription insertNewObjectForEntityForName:@"Pin" inManagedObjectContext:[DatabaseController managedObjectContext]];
            newPin.pinName = building[@"name"];
            newPin.pinDescription = building[@"description"];
            
            newPin.latitude = [NSNumber numberWithDouble:[[buidlingLocation objectForKey:@"latitude"] doubleValue]];
            newPin.longitude = [NSNumber numberWithDouble:[[buidlingLocation objectForKey:@"longitude"] doubleValue]];
            [DatabaseController save];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kAFN object:self];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}

+ (void) clearPinsFromDataBase
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: NSStringFromClass ([Pin class])];
    
    NSArray *results = [[DatabaseController managedObjectContext] executeFetchRequest: request error: nil];
    
    for (NSManagedObject *currentObject in results )
    {
        [[DatabaseController managedObjectContext] deleteObject: currentObject];
    }
}

+ (NSInteger) getPinCount
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: NSStringFromClass ([Pin class])];
    
    NSArray *results = [[DatabaseController managedObjectContext] executeFetchRequest: request error: nil];
    
    return [results count];
}


@end

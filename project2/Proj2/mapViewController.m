//
//  mapViewController.m
//  Proj2
//
//  Created by Lee on 10/24/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//

#import "mapViewController.h"

@interface mapViewController ()

@end

#define METERS_PER_MILE 1609.344

@implementation mapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    // 1
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = 43.613;
    zoomLocation.longitude= -116.2000;
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 2*METERS_PER_MILE, 2*METERS_PER_MILE);
    // 3
    [_mapView setRegion:viewRegion animated:YES];
    
}


- (void)updatePins
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: NSStringFromClass ([Pin class])];
    
    NSArray *results = [[DatabaseController managedObjectContext] executeFetchRequest: request error: nil];
    
    for (Pin *currentPin in results )
    {
        
        MKPointAnnotation *myAnnotation = [[MKPointAnnotation alloc] init];
        
        myAnnotation.title = currentPin.pinName;
        myAnnotation.subtitle = currentPin.pinDescription;
        
        CLLocationCoordinate2D pinCoordinate;
        pinCoordinate.latitude = [currentPin.latitude doubleValue];
        pinCoordinate.longitude = [currentPin.longitude doubleValue];
        
        myAnnotation.coordinate = pinCoordinate;
        
        [self.mapView addAnnotation:myAnnotation];
    }
}

- (void)viewDidLoad
{
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(updatePins) name: kAFN object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(refreshMap) name: kRefresh object: nil];
    
    [super viewDidLoad];
    
    [self updatePins];
}

-(void)refreshMap
{
    [self.mapView removeAnnotations:_mapView.annotations];
    [self updatePins];
}


- (IBAction)refreshButtonPushed:(id)sender
{
    [self.mapView removeAnnotations:_mapView.annotations];
    [PinController clearPinsFromDataBase];
    [PinController parseJSONForPins];
    [self updatePins];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

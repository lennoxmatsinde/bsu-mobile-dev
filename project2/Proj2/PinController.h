//
//  PinController.h
//  Proj2
//
//  Created by Lee on 10/25/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseController.h"
#import "Pin.h"
#import "AFNetworking.h"
#define kAFN @"kAFN"
#define kRefresh @"kRefresh"

@interface PinController : NSObject

+ (void) parseJSONForPins;
+ (void) clearPinsFromDataBase;
+ (NSInteger) getPinCount;


@end

//
//  DatabaseController.h
//  Proj2
//
//  Created by Lee on 10/24/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


static NSManagedObjectContext *managedObjectContext;
static NSPersistentStoreCoordinator *persistentStoreCoordinator;
static NSManagedObjectModel *managedObjectModel;

@interface DatabaseController : NSObject

+ (NSManagedObjectContext *) managedObjectContext;

+(void)save;

@end

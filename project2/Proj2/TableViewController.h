//
//  newTableViewController.h
//  Proj2
//
//  Created by Lee on 10/24/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Pin.h"
#import "DatabaseController.h"
#import "PinController.h"

@interface TableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

-(NSInteger)numberOfSectionsInTableView: (UITableView *)tableView;

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;

-(UITableViewCell *)tableView:(UITableView *)tableView cellforRowAtIndexPath:(NSIndexPath *)indexPath;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *refreshButtonHit;


@end

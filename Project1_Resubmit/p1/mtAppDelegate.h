//
//  mtAppDelegate.h
//  p1
//
//  Created by Lennox Matsinde on 12/16/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface mtAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

//
//  mapViewController.m
//  p1
//
//  Created by Lennox Matsinde on 12/16/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//


#import "mapViewController.h"


@interface mapViewController ()

@end

#define METERS_PER_MILE 1609.344

@implementation mapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    // 1
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = 43.6167;
    zoomLocation.longitude= -116.2000;
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 2*METERS_PER_MILE, 2*METERS_PER_MILE);
    // 3
    [_mapView setRegion:viewRegion animated:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://zstudiolabs.com/labs/compsci402/buildings.json" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        for (NSDictionary *building in responseObject){
            
            MKPointAnnotation *myAnnotation = [[MKPointAnnotation alloc] init];
            
            myAnnotation.title = building[@"name"];
            myAnnotation.subtitle = building[@"description"];
            
            NSDictionary *location = building[@"location"];
            NSLog(@"location %@", location);
            
            CLLocationCoordinate2D coordinate;
            coordinate.latitude = [[location objectForKey:@"latitude"] floatValue];
            coordinate.longitude = [[location objectForKey:@"longitude"] floatValue];
            
            myAnnotation.coordinate = coordinate;
            
            NSLog(@"coord %f, %f", coordinate.latitude, coordinate.longitude);
            NSLog(@"titles %@, %@", myAnnotation.title, myAnnotation.subtitle);
            
            [self.mapView addAnnotation:myAnnotation];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(createPin:) name: kPinCreated object: nil];
    
    if ([PinLocationFactory getPinArray]) {
        
        for (PinLocation *pin in [PinLocationFactory getPinArray]){
            [self.mapView addAnnotation: pin];
            
        }
    }

}

-(void) createPin: (NSNotification *) notification
{
    PinLocation *currentPin = [[PinLocationFactory getPinArray]lastObject];
    [self.mapView addAnnotation:currentPin];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

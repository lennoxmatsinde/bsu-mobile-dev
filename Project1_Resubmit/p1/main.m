//
//  main.m
//  p1
//
//  Created by Lennox Matsinde on 12/16/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//


#import <UIKit/UIKit.h>

#import "mtAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([mtAppDelegate class]));
    }
}

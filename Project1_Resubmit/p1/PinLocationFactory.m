//
//  PinLocationFactory.m
//  p1
//
//  Created by Lennox Matsinde on 12/16/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//


#import "PinLocationFactory.h"

@implementation PinLocationFactory

static NSMutableArray* pinArray;

+(PinLocation*) addPinLocationWithName: (NSString*)name PinDescription:(NSString*)pinDescription PinCoordinate:(CLLocationCoordinate2D)coordinate
{
    if (!pinArray) {
        pinArray = [[NSMutableArray alloc] init];
    }
    
    PinLocation *pin = [[PinLocation alloc] init];
    pin.pinName = name;
    pin.pinDescription = pinDescription;
    pin.pinCoordinate = coordinate;
    
    [pinArray addObject:pin];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kPinCreated object:self];
    
    return pin;
}

+(NSMutableArray*) getPinArray
{
    return [pinArray copy];
}



@end

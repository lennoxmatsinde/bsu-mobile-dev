//
//  PinLocationFactory.h
//  p1
//
//  Created by Lennox Matsinde on 12/16/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "PinLocation.h"

#define kPinCreated @"kPinCreated"

@interface PinLocationFactory : NSObject

+(PinLocation*) addPinLocationWithName: (NSString*)name PinDescription:(NSString*)description PinCoordinate:(CLLocationCoordinate2D)coordinate;

+(NSMutableArray*) getPinArray;

@end

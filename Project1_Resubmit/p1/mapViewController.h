//
//  mapViewController.h
//  p1
//
//  Created by Lennox Matsinde on 12/16/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "AFNetworking.h"
#import "PinLocationFactory.h"
#import "PinLocation.h"
#import "LocationController.h"


@interface mapViewController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *dropPinButton;

@end

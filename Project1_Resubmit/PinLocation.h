//
//  PinLocation.h
//  p1
//
//  Created by Lennox Matsinde on 12/16/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//


#import <MapKit/MapKit.h>
#import <Foundation/Foundation.h>

@interface PinLocation : NSObject <MKAnnotation>

@property (nonatomic, copy) NSString *pinName;
@property (nonatomic, copy) NSString *pinDescription;
@property (nonatomic, assign) CLLocationCoordinate2D pinCoordinate;

- (id)initWithName:(NSString*)name PinDescription:(NSString*)pinDescription PinCoordinate:(CLLocationCoordinate2D)coordinate;

@end



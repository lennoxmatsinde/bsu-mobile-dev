//
//  PinLocation.m
//  p1
//
//  Created by Lennox Matsinde on 12/16/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//

#import "PinLocation.h"

@interface PinLocation()

@end

@implementation PinLocation


- (id)initWithName:(NSString*)pinName PinDescription:(NSString*)pinDescription PinCoordinate:(CLLocationCoordinate2D)pinCoordinate
{
    
    self = [super init];
        if (self) {
    self.pinName = pinName;
    self.pinDescription = pinDescription;
    self.pinCoordinate = pinCoordinate;
        }
    
    return self;
}

- (NSString *)title {
    return _pinName;
}

- (NSString *)subtitle {
    return _pinDescription;
}

- (CLLocationCoordinate2D)coordinate {
    return _pinCoordinate;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

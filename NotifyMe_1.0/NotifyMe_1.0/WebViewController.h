//
//  WebViewController.h
//  NotifyMe_1.0
//
//  Created by Lee on 11/19/14.
//  Copyright (c) 2014 Scarib Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UIWebView *webSiteView;
@property (strong, nonatomic) IBOutlet NSURLRequest *urlRequest;
@end

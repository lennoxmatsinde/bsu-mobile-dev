//
//  MapViewController.h
//  NotifyMe
//
//  Created by Lee on 1/11/14.
//  Copyright (c) 2013 Scarib Tech. All rights reserved.
//


#import <UIKit/UIKit.h>
#include "Building.h"
#import "AFNetworking.h"

@interface MapViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end

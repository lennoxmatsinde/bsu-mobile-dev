<?PHP
#################################################################################
## Developed by Manifest Interactive, LLC                                      ##
## http://www.manifestinteractive.com                                          ##
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ##
##                                                                             ##
## THIS SOFTWARE IS PROVIDED BY MANIFEST INTERACTIVE 'AS IS' AND ANY           ##
## EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE         ##
## IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR          ##
## PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL MANIFEST INTERACTIVE BE          ##
## LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         ##
## CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        ##
## SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR             ##
## BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,       ##
## WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE        ##
## OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,           ##
## EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                          ##
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ##
## Author of file: Peter Schmalfeldt                                           ##
#################################################################################

/**
 * @category Apple Push Notification Service using PHP & MySQL
 * @package EasyAPNs
 * @author Peter Schmalfeldt <manifestinteractive@gmail.com>
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @link http://code.google.com/p/easyapns/
 */

/**
 * Begin Document
 */
if( $_GET['token'] != 'gobsu' || $_GET['token'] == '')
{
	return;
}

if($_POST['submit'])
{
	print 'Posted: '.$_POST['message'];
}
else
{
	?>
	<html>
		<body>
			<form action="send.php?token=gobsu" method="POST">
				Message: <input size="140" type="textfield" name="message" />
				Band: <input size="140" type="textfield" name="band" />
				<input type="hidden" name="token" value="gobsu" />
				<input type="submit" name="submit" />
			</form>
		<body>
	</html>
	<?php
	return;
}



// AUTOLOAD CLASS OBJECTS... YOU CAN USE INCLUDES IF YOU PREFER
if(!function_exists("__autoload")){ 
	function __autoload($class_name){
		require_once('APNS_src/php/classes/class_'.$class_name.'.php');
	// require('APNS_src/php/classes/class_DbConnect.php');
	}
}

// CREATE DATABASE OBJECT ( MAKE SURE TO CHANGE LOGIN INFO IN CLASS FILE )
$db = new DbConnect('localhost', 'root', 'root', 'NotifyMe');
$db->show_errors();


// FETCH $_GET OR CRON ARGUMENTS TO AUTOMATE TASKS
$apns = new APNS($db);


$pid_results = $db->query("SELECT `pid` from `apns_devices` where `pid`=1");
$deviceID_array = array();
while ($row = $pid_results->fetch_array() )
{
    array_push( $deviceID_array, $row['pid'] );
//    echo "pid: ".$row['pid'];
}

/**
/*	ACTUAL SAMPLES USING THE 'Examples of JSON Payloads' EXAMPLES (1-5) FROM APPLE'S WEBSITE.
 *	LINK:  http://developer.apple.com/iphone/library/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/ApplePushService/ApplePushService.html#//apple_ref/doc/uid/TP40008194-CH100-SW15
 */


$time = time() + (30);
$time_str = date('Y-m-d H:i:s', $time);

// APPLE APNS EXAMPLE 1
$apns->newMessage( $deviceID_array);//, $time_str );
$apns->addMessageAlert( $_POST['message'] );
//$apns->addMessageCustom( 'links', 'http://www.google.com' );
$apns->addMessageSound( 'jingle.caf' );
$apns->addMessageCustom( 'band',  $_POST['band'] );
$apns->addMessageCustom( 'service_request_id', '1720' );
$apns->addMessageCustom( 'kind', 'request' );
$apns->queueMessage();

// SEND ALL MESSAGES NOW
$apns->processQueue();

?>

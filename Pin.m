//
//  Pin.m
//  Proj2
//
//  Created by Lee on 10/25/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//

#import "Pin.h"

@implementation Pin

@dynamic pinName;
@dynamic pinDescription;
@dynamic latitude;
@dynamic longitude;


@end

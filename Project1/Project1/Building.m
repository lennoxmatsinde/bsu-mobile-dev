//
//  Building.m
//  Project1
//
//  Created by Lee on 9/24/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//

#import "Building.h"

@implementation Building

-(NSString *) name
{
    return _name;
    
}

-(NSString *) description
{
    return _descr;
}

-(double) longitude
{
    return _longitude;

}

-(double) latitude
{
    return _latitude;
}

//-(CLLocationCoordinate2D) coordinate
//{
//    return 0;
//}



@end

//
//  MapsViewController.m
//  03-Location
//
//  Created by Michael Ziray on 9/8/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "MapsViewController.h"
#import "Annotation.h"
#import "AFNetworking.h"
#import "LocationController.h"
#import <MapKit/MapKit.h>
#import "Building.h"
#import "newlocationViewController.h"
#define METERS_PER_MILE 1609.344


@interface MapsViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
//@property (weak, nonatomic) IBOutlet UIBarButtonItem *Description;


@end


#define BOISE_LATITUDE 43.60142;
#define BOISE_LONGITUDE -116.20079;;
#define THE_SPAN 0.01f;

@implementation MapsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    // 1
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = 43.60142;
    zoomLocation.longitude= -116.20079;
    
    // 7
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 1*METERS_PER_MILE, 1*METERS_PER_MILE);
    
    // 3
    [_mapView setRegion:viewRegion animated:YES];
    _mapView.showsUserLocation = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    MKCoordinateRegion myregion;
    CLLocationCoordinate2D center;
    center.latitude = BOISE_LATITUDE;
    center.longitude = BOISE_LONGITUDE;
    
    MKCoordinateSpan span;
    span.latitudeDelta = THE_SPAN;
    span.longitudeDelta = THE_SPAN;
    
    myregion.center = center;
    myregion.span = span;
    
    
    
    //[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(locationDidUpdate:) name: kLocationDidChange object: nil];
    
    //import afnetwork
    AFHTTPRequestOperationManager *manager =[AFHTTPRequestOperationManager manager];
    [manager GET:@"http://zstudiolabs.com/labs/compsci402/buildings.json"
        parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             //NSLog(@"JSON: %@", responseObject);
             
             Building *jsonResponse = [[Building alloc] init];
//             jsonResponse.name = responseObject[0][@"name"];
//             NSLog(@"%@",jsonResponse.name);
             
             for (NSDictionary* myresponse in responseObject)
             {
                 jsonResponse.name = myresponse[@"name"];
                 jsonResponse.descr = myresponse[@"description"];
                 jsonResponse.longitude = [myresponse[@"location"][@"longitude"] doubleValue];
                 jsonResponse.latitude = [myresponse[@"location"][@"latitude"] doubleValue];
                 [ self pintomap:jsonResponse ]; //workin progress
                                  
                 
                 NSLog(@"%@",myresponse);
             }

             
             NSMutableArray *currCoords2 = [[NSMutableArray alloc] init];
             currCoords2 = [newlocationViewController getpinarray];
             Annotation * newAnnotation = [Annotation alloc];
             newAnnotation = [currCoords2 objectAtIndex:0];
             [self.mapView addAnnotation:newAnnotation];


             
             
             
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error){
             NSLog(@"Error: %@", error);
         }
     ];
    
    
    
    
    
    

}





-(void) pintomap:(Building *)jsonResponse
{
    
    //Annotation related code
    //create coordinate for use with annotation
    
    
    
    CLLocationCoordinate2D buildinglocation;
    buildinglocation.longitude = jsonResponse.longitude;
    buildinglocation.latitude = jsonResponse.latitude;
    
    
    Annotation * myAnnotation = [Annotation alloc];
    myAnnotation.coordinate = buildinglocation;
    myAnnotation.title = jsonResponse.name;
    myAnnotation.subtitle = jsonResponse.descr;
    
    
    [self.mapView addAnnotation:myAnnotation];
    
    //create anotehr array here which will receive the other array and enable me to pull out the mkannotation
    //call getpinarray methos from here, when other pins are places do



}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end





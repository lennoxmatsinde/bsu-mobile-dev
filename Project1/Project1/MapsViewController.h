//
//  MapsViewController.h
//  03-Location
//
//  Created by Michael Ziray on 9/8/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "Building.h"
#define kLocationDidChange @"kLocationDidChange"
@interface MapsViewController : UIViewController
- (void) pintomap: (Building *) jsonResponse;
@end

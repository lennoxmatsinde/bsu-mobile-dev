//
//  Building.h
//  Project1
//
//  Created by Lee on 9/24/14.
//  Copyright (c) 2014 Lee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Building : NSObject //<>

@property(nonatomic, retain) NSString * name;
@property(nonatomic, retain) NSString * descr;
@property(nonatomic) double latitude;
@property(nonatomic) double longitude;
//@property(nonatomic, assign) CLLocationCoordinate2D buildingLocation;


-(NSString *) name;
-(NSString *) descr;
-(double ) latitude;
-(double ) longitude;

@end
